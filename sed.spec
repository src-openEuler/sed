Name:	 sed
Version: 4.9
Release: 4
Summary: non-interactive command-line text editor

License: GPLv3+
URL:	 https://www.gnu.org/software/sed/
Source0: http://ftp.gnu.org/gnu/sed/%{name}-%{version}.tar.xz

Patch1:  backport-sed-c-flag.patch
Patch2:  backport-sed-fix-symlink-bufsize-readlink-check.patch

BuildRequires: gzip automake autoconf gcc
BuildRequires: glibc-devel libselinux-devel libacl-devel perl-Getopt-Long
Provides: /bin/sed
Provides: bundled(gnulib)

%description
Sed is a non-interactive command-line text editor. A stream editor is used
to per-form basic text transformations on an input stream (a file or input
from a pipeline).

%package        help
Summary:        Documents for %{name}
Buildarch:      noarch
Requires:	man info

%description help
Man pages and other related documents for %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --without-included-regex
make %{?_smp_mflags}

%check
make check

%install
%make_install
%find_lang %{name}

%files -f %{name}.lang
%license COPYING
%{_bindir}/sed

%files help
%doc ABOUT-NLS AUTHORS BUGS ChangeLog* INSTALL NEWS README THANKS
%{_infodir}/*.info.gz
%exclude %{_infodir}/dir*
%{_mandir}/man1/*.1.gz

%changelog
* Tue Apr 30 2024 kouwenqi <kouwenqi@kylinos.cn> - 4.9-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix symlink bufsize readlink check

* Sun Apr 23 2023 zhangruifang <zhangruifang1@h-partners.com> - 4.9-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Modify the check based on the spec specifications

* Fri Apr 14 2023 fuanan <fuanan3@h-partners.com> - 4.9-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:spec file add make check

* Sat Jan 28 2023 yixiangzhike<yixiangzhike007@163.com> - 4.9-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade software to v4.9

* Mon Feb 8 2021 yangzhuangzhuang<yangzhuangzhuang1@huawei.com> - 4.8-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:backport patches from upstream

* Thu Apr 16 2020 chengquan<chengquan3@huawei.com> - 4.8-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade software to v4.8

* Tue Jan  7 2020 JeanLeo<liujianliu.liu@huawei.com> - 4.7-0
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update software to version 4.7

* Fri Aug 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.5-3
- Package init
